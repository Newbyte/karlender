use crate::store::{gstore::*, store, State};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::prelude::*;

#[widget(extends gtk::Box)]
#[template(file = "nav.ui")]
pub struct NavHeaderButton {
    #[template_child]
    pub prev_button: TemplateChild<gtk::Button>,
    #[template_child]
    pub next_button: TemplateChild<gtk::Button>,
}

impl NavHeaderButton {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create NavHeaderButton")
    }

    pub fn constructed(&self) {
        let s = self;
        let prev_button = &self.imp().prev_button;
        let next_button = &self.imp().next_button;

        select!(|state| state.ui => glib::clone!(@weak s => move |state| {
            let mobile = state.ui.mobile;
            let page = &state.ui.current_page;
            s.set_visible(!mobile && page == crate::pages::DAY_PAGE_NAME);
        }));

        prev_button.connect_clicked(|_b| {
            dispatch!(crate::store::_GO_PREV);
        });
        next_button.connect_clicked(|_b| {
            dispatch!(crate::store::_GO_NEXT);
        });
    }
}

impl Default for NavHeaderButton {
    fn default() -> Self {
        Self::new()
    }
}
