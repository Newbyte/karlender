use crate::store::{gstore::select, store, State};
use gdk4::subclass::prelude::ObjectSubclassIsExt;

use gtk::prelude::*;

#[widget(extends gtk::Box)]
#[template(file = "spinner.ui")]
pub struct SpinnerHeaderButton {
    #[template_child]
    pub spinner: TemplateChild<gtk::Spinner>,
}

impl SpinnerHeaderButton {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create SpinnerHeaderButton")
    }
    pub fn constructed(&self) {
        let s = self;
        select!(|state| state.ui => glib::clone!(@weak s => move |state| {
            let syncing = state.ui.syncing;
            s.set_visible(syncing);
            // if syncing {
            // } else {
            //     spinner
            // }
        }));
    }
}

impl Default for SpinnerHeaderButton {
    fn default() -> Self {
        Self::new()
    }
}
