use crate::store::{
    gstore::{select_state, DispatchWidget},
    store, State,
};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::prelude::*;

#[widget(extends gtk::Box)]
#[template(file = "new_event.ui")]
pub struct NewEventHeaderButton {
    #[template_child]
    pub button: TemplateChild<gtk::Button>,
}

impl NewEventHeaderButton {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create NewEventHeaderButton")
    }
    pub fn constructed(&self) {
        let s = self;
        let button = &self.imp().button;

        select_state!(
            "ui and selection",
            |state| {
                let mut selected_state = State::default();
                selected_state.ui.current_page = state.ui.current_page.clone();
                selected_state.selection_slice = state.selection_slice.clone();
                selected_state
            },
            glib::clone!(@weak s => move |state| {
                let current_page = &state.ui.current_page;
                let selection = &state.selection_slice;

                if current_page == "month" {
                    s.set_visible(true);
                    if selection.selection.is_some() {
                        s.set_sensitive(true);
                    } else {
                        s.set_sensitive(false);
                    }
                } else {
                    s.set_visible(false)
                }
            })
        );

        button.connect_clicked(|b| {
            b.dispatch(crate::store::NEW_EVENT, None);
        });
    }
}

impl Default for NewEventHeaderButton {
    fn default() -> Self {
        Self::new()
    }
}
