use gtk::prelude::*;

#[widget(extends gtk::Box)]
#[template(file = "menu.ui")]
pub struct MenuHeaderButton {}

impl MenuHeaderButton {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create menu button")
    }
    pub fn constructed(&self) {
        //
    }
}

impl Default for MenuHeaderButton {
    fn default() -> Self {
        Self::new()
    }
}
