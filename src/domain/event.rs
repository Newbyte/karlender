use std::collections::HashMap;

use glib::StaticVariantType;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use super::{CalendarId, DatetimeFormat, KarlenderDate, KarlenderDateTime};

pub type Repeat = Vec<(String, String)>;
pub type EventId = String;

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq, Eq)]
pub enum SyncStatus {
    Synced(String),
    LocallyModified(String),
    LocallyDeleted(String),
    NotSynced,
}

#[derive(Clone, Deserialize, Serialize, PartialEq, Eq)]
#[variant_serde_json]
pub struct Event {
    pub id: EventId,
    pub calendar_url: CalendarId,
    pub item_url: String,
    pub name: String,
    pub location: Option<String>,

    pub start: KarlenderDateTime,
    pub end: KarlenderDateTime,
    pub full_day: bool,

    pub notes: Option<String>,

    pub repeat: Option<Repeat>,

    pub sync_status: SyncStatus,
}

impl Event {
    pub fn new(
        calendar_url: String,
        start: KarlenderDateTime,
        end: KarlenderDateTime,
        full_day: bool,
    ) -> Self {
        let id = Uuid::new_v4().to_string();
        let item_url = format!("{}/{}", calendar_url, id);
        let item_url = item_url.replace("//", "/");
        Event {
            id,
            calendar_url,
            item_url,
            name: "New Event".into(),
            location: None,
            start,
            end,
            full_day,
            notes: None,
            repeat: None,
            sync_status: SyncStatus::NotSynced,
        }
    }
}

impl Event {
    pub fn contains_date(&self, date: &KarlenderDate) -> bool {
        if !self.repeats("YEARLY") && self.start.year() != date.year() {
            return false;
        }
        if !self.repeats("MONTHLY") && self.start.month() != date.month() {
            return false;
        }
        if self.start.day() == self.end.day() && date.day() == self.start.day() {
            return true;
        }
        if date.day() >= self.start.day() && date.day() < self.end.day() {
            return true;
        }
        false
    }

    pub fn repeats(&self, freq: &str) -> bool {
        let repeat = self.repeat();
        match repeat.get(&"FREQ".to_string()) {
            Some(v) => v.as_str() == freq,
            None => false,
        }
    }

    pub fn repeat(&self) -> HashMap<&String, &String> {
        let mut map = HashMap::new();
        if let Some(repeat) = self.repeat.as_ref() {
            for (k, v) in repeat {
                map.insert(k, v);
            }
        }
        map
    }

    pub fn is_between(&self, start: KarlenderDateTime, end: KarlenderDateTime) -> bool {
        self.start >= start && self.end <= end
    }
}

impl std::fmt::Debug for Event {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Event")
            .field("id", &self.id)
            .field("calendar_url", &self.calendar_url)
            .field("item_url", &self.item_url)
            .field("name", &self.name)
            .field("location", &self.location)
            .field("start", &self.start.format(DatetimeFormat::ISO8601Z))
            .field("end", &self.start.format(DatetimeFormat::ISO8601Z))
            .field("full_day", &self.full_day)
            .field("notes", &self.notes)
            .field("repeat", &self.repeat)
            .field("sync_status", &self.sync_status)
            .finish()
    }
}
