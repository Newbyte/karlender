use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, Deserialize, Serialize, PartialEq, Eq)]
pub enum KarlenderColor {
    Red,
    Green,
    Blue,
    Orange,
    Black,
}

impl KarlenderColor {
    pub fn style_class(&self) -> &'static str {
        match self {
            KarlenderColor::Red => "calendar-color-red",
            KarlenderColor::Green => "calendar-color-green",
            KarlenderColor::Blue => "calendar-color-blue",
            KarlenderColor::Orange => "calendar-color-orange",
            KarlenderColor::Black => "calendar-color-black",
        }
    }
}

impl From<String> for KarlenderColor {
    fn from(s: String) -> Self {
        match s.as_str() {
            "Red" => KarlenderColor::Red,
            "Green" => KarlenderColor::Green,
            "Blue" => KarlenderColor::Blue,
            "Orange" => KarlenderColor::Orange,
            "Black" => KarlenderColor::Black,
            _ => KarlenderColor::Black,
        }
    }
}

impl ToString for KarlenderColor {
    fn to_string(&self) -> String {
        match self {
            KarlenderColor::Red => "Red".into(),
            KarlenderColor::Green => "Green".into(),
            KarlenderColor::Blue => "Blue".into(),
            KarlenderColor::Orange => "Orange".into(),
            KarlenderColor::Black => "Black".into(),
        }
    }
}
