use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use uuid::Uuid;

use super::{Event, EventId, KarlenderColor};

pub type CalendarId = String;

pub const CALENDAR_PROP_NAME: &str = "name";
pub const CALENDAR_PROP_ENABLED: &str = "enabled";
pub const CALENDAR_PROP_DEFAULT: &str = "default";
pub const CALENDAR_PROP_COLOR: &str = "color";

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq, Eq)]
#[gobject(id, name)]
pub struct Calendar {
    pub id: String,
    pub name: String,
    pub events: HashMap<EventId, Event>,
    pub enabled: bool,
    pub default: bool,
    pub color: KarlenderColor,
    pub is_remote: bool,
}

impl Default for Calendar {
    fn default() -> Self {
        Self {
            id: Uuid::new_v4().to_string(),
            name: "New Calendar".into(),
            events: Default::default(),
            enabled: true,
            default: false,
            color: KarlenderColor::Black,
            is_remote: false,
        }
    }
}
