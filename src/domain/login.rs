use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct Login {
    pub url: String,
    pub login: String,
}

pub type Password = String;
