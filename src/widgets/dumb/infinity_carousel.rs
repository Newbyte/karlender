use gdk4::subclass::prelude::{ObjectImpl, ObjectSubclassIsExt};
use gtk::prelude::*;
use libadwaita as adw;
use std::cell::Cell;

type Creator = Box<dyn Fn(i64) -> gtk::Widget + 'static>;

#[widget(extends gtk::Box)]
#[template(file = "infinity_carousel.ui")]
struct InfinityCarousel {
    #[property_i64]
    pub lower: Cell<i64>,
    #[property_i64]
    pub upper: Cell<i64>,
    #[property_i64]
    pub current: Cell<i64>,

    #[property_u64]
    pub initial_page: Cell<u64>,

    pub item: Cell<Option<Creator>>,

    #[template_child]
    pub carousel: TemplateChild<adw::Carousel>,

    #[signal]
    pub page_changed: (),
}

impl InfinityCarousel {
    pub fn new(creator: impl Fn(i64) -> gtk::Widget + 'static) -> Self {
        let carousel: Self = glib::Object::new(&[
            //
            ("lower", &-3i64),
            ("upper", &3i64),
            ("initial-page", &3u64),
        ])
        .expect("Failed to create month page");
        carousel.imp().item.set(Some(Box::new(creator)));
        Self::constructed(&carousel);
        carousel
    }

    pub fn constructed(&self) {
        if let Some(item) = self.imp().item.take() {
            let s = self;
            let lower = self.imp().lower.clone().take();
            let upper = self.imp().upper.clone().take();

            let carousel = &self.imp().carousel;
            let initial_page = self.imp().initial_page.clone().take();

            // TODO: Create more calendar pages async

            for i in lower..upper + 1 {
                self.imp().carousel.append(&item(i));
            }

            carousel.connect_realize(move |carousel| {
                let (sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
                let carousel = carousel.clone();
                receiver.attach(None, move |_| {
                    carousel.scroll_to(&carousel.nth_page(initial_page as u32), false);
                    glib::Continue(false)
                });
                std::thread::spawn(move || {
                    std::thread::sleep(std::time::Duration::from_millis(1000));
                    sender.send(())
                });
            });

            carousel.connect_page_changed(glib::clone!(@weak s => move |carousel, n| {
                s.imp().current.set(n as i64);
                let upper = s.imp().upper.clone().take();
                let lower = s.imp().lower.clone().take();

                s.on_page_changed(n);

                let len = upper - lower;
                if n as i32 <= 1 {
                    let lower = lower - 1;
                    carousel.prepend(&item(lower));
                    s.imp().lower.set(lower);
                } else if n >= len as u32 - 2 {
                    let upper = upper + 1;
                    carousel.append(&item(upper));
                    s.imp().upper.set(upper);
                }
            }));
        }
    }

    pub fn current_page<T: IsA<gtk::Widget>>(&self) -> T {
        let nth = self.imp().current.clone().take();
        self.imp().carousel.nth_page(nth as u32).downcast().unwrap()
    }

    pub fn connect_page_changed(&self, f: impl Fn(&Self) + 'static) {
        self._connect_page_changed(f);
    }

    fn on_page_changed(&self, _: u32) {
        self.emit_page_changed();
    }
}
