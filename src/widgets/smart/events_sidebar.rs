use crate::store::{gstore::*, *};
use crate::widgets::EventRow;
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::prelude::*;

#[widget(extends gtk::Box)]
#[template(file = "events_sidebar.ui")]
pub struct EventsSidebar {
    #[template_child]
    pub event_box: TemplateChild<gtk::Box>,
}
impl EventsSidebar {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create EventsSidebar")
    }
    pub fn constructed(&self) {
        let s = self;

        select!(|state|
                state.calendar_slice,
                state.selection_slice,
                state.settings,
            => glib::clone!(@weak s => move |state| {
                let view_events = &state.selection_slice.view_events;
                let calendars = &state.calendar_slice.calendars;
                let dateformat = &state.settings.dateformat;
                let timezone = state.settings.timezone();
                let event_box = &s.imp().event_box;
                let mut events = Vec::new();
                for (calendar_id, event_id) in view_events {
                    if let Some(calendar) = calendars.get(calendar_id) {
                        if let Some(event) = calendar.events.get(event_id) {
                            events.push((calendar_id.clone(), event));
                        }
                    }
                }
                events.sort_by(|(_, a), (_, b)| b.start.cmp(&a.start));
                while let Some(child) = event_box.first_child() {
                    event_box.remove(&child);
                }
                for (calendar_id, event) in events {
                    event_box.prepend(&EventRow::new(
                        dateformat.clone(),
                        timezone.clone(),
                        &calendar_id,
                        event,
                        true)
                    );
                }
            })
        );
    }
}

impl Default for EventsSidebar {
    fn default() -> Self {
        Self::new()
    }
}
