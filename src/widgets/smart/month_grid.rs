use crate::domain::*;
use crate::store::{gstore::*, store, State};
use crate::widgets::{MonthCell, MonthHeaderCell};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gettextrs::gettext;
use glib::subclass::signal::Signal;
use gtk::prelude::*;
use std::cell::Cell;

#[widget(extends gtk::Box)]
#[template(file = "month_grid.ui")]
pub struct MonthGrid {
    #[template_child]
    pub grid: TemplateChild<gtk::Grid>,
    #[template_child]
    pub cell_size_group: TemplateChild<gtk::SizeGroup>,
    #[template_child]
    pub title_label: TemplateChild<gtk::Label>,

    #[property_i64]
    pub delta: Cell<i64>,
    #[property_string]
    pub title: Cell<String>,

    #[signal(Signal::builder(
        "day-selected",
         &[
             String::static_type().into(),
             bool::static_type().into(),
             i32::static_type().into()
         ],
         <()>::static_type().into()).build())]
    pub day_selected: (),
}

impl MonthGrid {
    pub fn new(delta: i64) -> Self {
        glib::Object::new(&[
            //
            ("delta", &delta),
        ])
        .expect("Failed to create MonthGrid")
    }

    pub fn constructed(&self) {
        self.connect_realize(|s| {
            s.realized();
        });
        let s = self;
        select!(|state| state.ui.mobile => glib::clone!(@weak s => move |state| {
            if state.ui.mobile {
                s.imp().title_label.set_visible(false);
            } else {
                s.imp().title_label.set_visible(true);
            }
        }));
    }

    pub fn realized(&self) {
        let s = self;
        let delta_month: i64 = self.imp().delta.clone().take();
        let cell_size_group = &self.imp().cell_size_group;

        let today_month = KarlenderTimezone::local().today();

        // month for this grid
        let this_month = get_month_by_delta(&today_month, delta_month);
        let this_month_first_day = this_month.with_day(1).unwrap();

        self.set_widget_name(&format!("month-{}", this_month.month()));

        let year = this_month_first_day.format(DatetimeFormat::Year);
        let month = this_month_first_day.month();
        self.set_property("title", format!("{} {}", year, get_mont_by_num(month)));

        let month_grid = &self.imp().grid;

        for i in 0..7 {
            month_grid.attach(&MonthHeaderCell::new(i), i as i32, 0, 1, 1);
        }

        let former_month = get_month_by_delta(&this_month, -1);
        let former_month_len = get_month_len(&former_month);
        let next_month = get_month_by_delta(&this_month, 1);
        let this_month_len = get_month_len(&this_month);
        let this_month_start = this_month_first_day.weekday().number_from_monday();

        let mut whole_week_not_in_month = false;
        for week in 0..6 {
            for day in 0..7 {
                let day_num = day + (week * 7);

                let (date, in_current_month) = if day_num < this_month_start {
                    (
                        former_month
                            .with_day0(former_month_len - (this_month_start - day_num))
                            .unwrap(),
                        false,
                    )
                } else if day_num - this_month_start >= this_month_len {
                    (
                        next_month
                            .with_day0(day_num - this_month_start - this_month_len)
                            .unwrap(),
                        false,
                    )
                } else {
                    (
                        this_month.with_day0(day_num - this_month_start).unwrap(),
                        true,
                    )
                };

                if week > 0 && day == 0 && !in_current_month || whole_week_not_in_month {
                    whole_week_not_in_month = true;
                    continue;
                }

                let cell = MonthCell::new(date, in_current_month);
                cell.connect_clicked(glib::clone!(@weak s => move |cell, num_clicks| {
                    if cell.style_context().has_class("cell-selected") {
                        cell.style_context().remove_class("cell-selected");
                        s.do_emit_day_selected(cell.date().and_hms(0, 0, 0, None).to_string(), false, num_clicks);
                    } else {
                        cell.style_context().add_class("cell-selected");
                        s.do_emit_day_selected(cell.date().and_hms(0, 0, 0, None).to_string(), true, num_clicks);
                    }
                }));

                self.connect_day_selected(
                    glib::clone!(@weak cell => move |_self, d, _s, _num_clicks| {
                        if cell.date().and_hms(0, 0, 0, None).to_string() != d {
                            cell.style_context().remove_class("cell-selected");
                        }
                    }),
                );

                cell_size_group.add_widget(&cell);

                month_grid.attach(&cell, day as i32, 1 + week as i32, 1, 1);
            }
        }
    }

    fn do_emit_day_selected(&self, date: String, selected: bool, num_clicks: i32) {
        self.emit_by_name(
            "day-selected",
            &[
                &date.to_value(),
                &selected.to_value(),
                &num_clicks.to_value(),
            ],
        )
    }

    pub fn title(&self) -> String {
        let title = self.imp().title.take();
        self.imp().title.set(title.clone());
        title
    }

    pub fn connect_day_selected(&self, f: impl Fn(&Self, String, bool, i32) + 'static) {
        self.connect_closure(
            "day-selected",
            false,
            closure_local!(
                move |s: Self, date: String, selected: bool, num_clicks: i32| {
                    f(&s, date, selected, num_clicks);
                }
            ),
        );
    }
}

fn get_mont_by_num(num: u8) -> String {
    match num {
        1 => gettext("January"),
        2 => gettext("February"),
        3 => gettext("March"),
        4 => gettext("April"),
        5 => gettext("May"),
        6 => gettext("June"),
        7 => gettext("July"),
        8 => gettext("August"),
        9 => gettext("September"),
        10 => gettext("October"),
        11 => gettext("November"),
        12 => gettext("December"),
        _ => "Unknown".into(),
    }
}
