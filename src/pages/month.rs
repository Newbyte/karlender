use crate::domain::KarlenderDateTimeDuration;
use crate::store::selection::Selection;
use crate::store::{gstore::*, store};
use crate::{
    domain::KarlenderDateTime,
    widgets::{InfinityCarousel, MonthGrid},
};

use gettextrs::gettext;
use gtk::prelude::*;
use gtk_rust_app::widgets::Page;

pub const MONTH_PAGE_NAME: &str = "month";

#[widget(extends gtk::Box)]
#[template(file = "month.ui")]
struct MonthPage {}

impl MonthPage {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create month page")
    }
    pub fn constructed(&self) {
        let carousel = InfinityCarousel::new(|i| {
            let mg = MonthGrid::new(i);
            mg.connect_day_selected(|_g, date, selected, num_clicks| {
                let date = KarlenderDateTime::from_str(&date).unwrap().date();
                let start = date.and_hms(0, 0, 0, None);
                let end = date.and_hms(0, 0, 0, None) + KarlenderDateTimeDuration::days(1);
                let full_day = true;
                let selection = Selection::new(start, end, full_day, selected, num_clicks);
                dispatch!(crate::store::SELECT, selection.to_variant());
            });
            mg.upcast()
        });

        carousel.connect_page_changed(|ic| {
            let page: MonthGrid = ic.current_page();
            let title = page.title();
            dispatch!(
                crate::store::_NAVIGATE_WITH_TITLE,
                (MONTH_PAGE_NAME.to_string(), title)
            );
        });

        self.append(&carousel);
    }

    pub fn monthly_title(&self) -> String {
        let c: InfinityCarousel = self.first_child().unwrap().downcast().unwrap();
        let mg: MonthGrid = c.current_page();
        mg.title()
    }
}

impl Page for MonthPage {
    fn name(&self) -> &'static str {
        MONTH_PAGE_NAME
    }

    fn title_and_icon(&self) -> Option<(String, String)> {
        Some((gettext("Month"), "month-symbolic".into()))
    }
}

impl Default for MonthPage {
    fn default() -> Self {
        Self::new()
    }
}
