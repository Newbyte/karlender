use crate::store::calendars::LOGIN_PASSWORD;
use crate::store::{gstore::*, store, NAVIGATE_BACK};

use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::prelude::*;
use gtk_rust_app::widgets::Page;
use libadwaita as adw;

pub const ADD_ACCOUNT_PAGE_NAME: &str = "add_account";

#[widget(extends gtk::Box)]
#[template(file = "add_account.ui")]
pub struct AddAccountPage {
    #[template_child]
    pub login_inputs_size_group: TemplateChild<gtk::SizeGroup>,

    #[template_child]
    pub caldav_url_row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub caldav_url_entry: TemplateChild<gtk::Entry>,
    #[template_child]
    pub username_row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub username_entry: TemplateChild<gtk::Entry>,
    #[template_child]
    pub password_row: TemplateChild<adw::ActionRow>,
    #[template_child]
    pub password_entry: TemplateChild<gtk::PasswordEntry>,

    #[template_child]
    pub login_button: TemplateChild<gtk::Button>,

    #[callback]
    on_login: (),
}

impl AddAccountPage {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create AddAcountPage")
    }

    pub fn constructed(&self) {
        self.setup_login_size_group();
    }

    fn on_login(&self, _: gtk::Button) {
        let username = self.imp().username_entry.text().to_string();
        self.imp().username_entry.set_text("");
        let password = self.imp().password_entry.text().to_string();
        self.imp().password_entry.set_text("");
        let url = self.imp().caldav_url_entry.text().to_string();
        self.imp().caldav_url_entry.set_text("");

        // Do not send the password around. It must not be logged or smth.
        let mutex = LOGIN_PASSWORD.get_or_init(Default::default);
        let mut lock = mutex.lock().unwrap();
        *lock = Some(password);
        drop(lock);

        dispatch!(crate::store::LOGIN, (url, username));
        dispatch!(crate::store::NAVIGATE, NAVIGATE_BACK);
    }

    fn setup_login_size_group(&self) {
        self.imp().login_inputs_size_group.add_widget(
            &self
                .imp()
                .caldav_url_row
                .child()
                .unwrap()
                .last_child()
                .unwrap()
                .prev_sibling()
                .unwrap(),
        );
        self.imp().login_inputs_size_group.add_widget(
            &self
                .imp()
                .username_row
                .child()
                .unwrap()
                .last_child()
                .unwrap()
                .prev_sibling()
                .unwrap(),
        );
        self.imp().login_inputs_size_group.add_widget(
            &self
                .imp()
                .password_row
                .child()
                .unwrap()
                .last_child()
                .unwrap()
                .prev_sibling()
                .unwrap(),
        );
    }
}

impl Page for AddAccountPage {
    fn name(&self) -> &'static str {
        ADD_ACCOUNT_PAGE_NAME
    }

    fn title_and_icon(&self) -> Option<(String, String)> {
        None
    }
}

impl Default for AddAccountPage {
    fn default() -> Self {
        Self::new()
    }
}
