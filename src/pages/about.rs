use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::{prelude::*, subclass::prelude::TemplateChild};
use gtk_rust_app::widgets::Page;

pub const ABOUT_PAGE_NAME: &str = "about";

#[widget(extends gtk::Box)]
#[template(file = "about.ui")]
pub struct AboutPage {
    #[template_child]
    pub appname_label: TemplateChild<gtk::Label>,
    #[template_child]
    pub version_label: TemplateChild<gtk::Label>,
}

impl AboutPage {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create about page")
    }
    pub fn constructed(&self) {
        self.imp().appname_label.set_label("Karlender");
        self.imp()
            .version_label
            .set_label(env!("CARGO_PKG_VERSION"));
    }
}

impl Page for AboutPage {
    fn name(&self) -> &'static str {
        ABOUT_PAGE_NAME
    }

    fn title_and_icon(&self) -> Option<(String, String)> {
        None
    }
}

impl Default for AboutPage {
    fn default() -> Self {
        Self::new()
    }
}
