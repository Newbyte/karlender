use super::Adapter;
use crate::domain::*;
use gettextrs::gettext;
use glib::once_cell::sync::OnceCell;
use secret_service::SecretService;
use std::collections::HashMap;
use url::Url;

pub const RRULE_FIELD_FREQ: &str = "FREQ";
pub const RRULE_VALUE_YEARLY: &str = "YEARLY";
pub const RRULE_VALUE_MONTHLY: &str = "MONTHLY";
pub const RRULE_VALUE_WEEKLY: &str = "WEEKLY";
pub const RRULE_VALUE_DAILY: &str = "DAILY";
pub const RRULE_VALUE_HOURLY: &str = "HOURLY";

// pub const RRULE_FIELD_BYMONTH: &str = "BYMONTH";

// pub const RRULE_FIELD_BYMONTHDAY: &str = "BYMONTHDAY";

pub const RRULE_FIELD_BYDAY: &str = "BYDAY";
pub const RRULE_VALUE_BYDAY_MONDAY: &str = "MO";
pub const RRULE_VALUE_BYDAY_TUESDAY: &str = "TU";
pub const RRULE_VALUE_BYDAY_WEDNESDAY: &str = "WE";
pub const RRULE_VALUE_BYDAY_THURSDAY: &str = "TH";
pub const RRULE_VALUE_BYDAY_FRIDAY: &str = "FR";
pub const RRULE_VALUE_BYDAY_SATURDAY: &str = "SA";
pub const RRULE_VALUE_BYDAY_SUNDAY: &str = "SU";

// pub const RRULE_FIELD_BYSETPOS: &str = "BYSETPOS";
// pub const RRULE_VALUE_BYSETPOS_FIRST: &str = "1";
// pub const RRULE_VALUE_BYSETPOS_SECOND: &str = "2";
// pub const RRULE_VALUE_BYSETPOS_THIRD: &str = "3";
// pub const RRULE_VALUE_BYSETPOS_FOURTH: &str = "4";
// pub const RRULE_VALUE_BYSETPOS_LAST: &str = "-1";

pub const RRULE_FIELD_INTERVAL: &str = "INTERVAL";

pub const RRULE_FIELD_COUNT: &str = "COUNT";
pub const RRULE_FIELD_UNTIL: &str = "UNTIL";
pub trait CaldavAdapterApi: Adapter {
    fn login(url: String, login: String, password: Password) -> Option<Login>;
    fn get_login() -> Option<(Login, Password)>;
    fn remove_login();
    fn sync(sender: glib::Sender<Sync>, calendars: &HashMap<CalendarId, Calendar>);
    fn remove_event(event: &Event);
    fn save_event(event: Event) -> Option<Event>;
}

#[allow(dead_code)]
#[derive(Debug)]
pub enum Sync {
    Started,
    Progress,
    Success(HashMap<CalendarId, Calendar>),
    Failure(String),
}

static SECRET_SERVICE: OnceCell<Result<SecretService, secret_service::Error>> = OnceCell::new();

pub struct CaldavAdapter;
impl Adapter for CaldavAdapter {
    fn init() {}
}
impl CaldavAdapterApi for CaldavAdapter {
    fn login(url: String, login: String, password: Password) -> Option<Login> {
        Self::remove_login();

        let service =
            SECRET_SERVICE.get_or_init(|| SecretService::new(secret_service::EncryptionType::Dh));
        if let Err(e) = service {
            error!("Could not access secret-service API: {:?}", e);
            return None;
        }
        let service = service.as_ref().unwrap();
        if let Ok(c) = service.get_default_collection() {
            let mut attributes = HashMap::new();
            attributes.insert("label", "karlender");
            attributes.insert("url", &url);
            attributes.insert("login", &login);

            if let Err(e) = c.create_item(
                "karlender",
                attributes,
                password.as_bytes(),
                true,
                "Password",
            ) {
                error!("Could not store secret: {:?}", e);
            } else {
                return Some(Login { url, login });
            }
        }
        None
    }

    fn remove_login() {
        let service =
            SECRET_SERVICE.get_or_init(|| SecretService::new(secret_service::EncryptionType::Dh));
        if let Err(e) = service {
            error!("Could not access secret-service API: {:?}", e);
            return;
        }
        let service = service.as_ref().unwrap();
        if let Ok(c) = service.get_default_collection() {
            let mut attributes = HashMap::new();
            attributes.insert("label", "karlender");

            if let Ok(items) = c.search_items(attributes) {
                for item in items {
                    item.delete().unwrap();
                }
            }
        }
    }

    fn get_login() -> Option<(Login, Password)> {
        let service =
            SECRET_SERVICE.get_or_init(|| SecretService::new(secret_service::EncryptionType::Dh));
        if let Err(e) = service {
            error!("Could not access secret-service API: {:?}", e);
            return None;
        }
        let service = service.as_ref().unwrap();
        if let Ok(c) = service.get_default_collection() {
            let mut attributes = HashMap::new();
            attributes.insert("label", "karlender");
            if let Ok(items) = c.search_items(attributes) {
                if let Some(item) = items.first() {
                    let attr = item.get_attributes().unwrap();
                    let url = attr.get(&"url".to_string()).unwrap();
                    let email = attr.get(&"login".to_string()).unwrap();
                    let password = String::from_utf8(item.get_secret().unwrap()).unwrap();
                    return Some((
                        Login {
                            url: url.clone(),
                            login: email.clone(),
                        },
                        password,
                    ));
                }
            }
        }
        None
    }

    fn sync(sender: glib::Sender<Sync>, calendars: &HashMap<CalendarId, Calendar>) {
        debug!("Start syncing");
        let mut merged_calendars = calendars.clone();
        std::thread::spawn(move || {
            sender
                .send(Sync::Started)
                .expect("Faild to send sync update");

            if let Some((login, password)) = Self::get_login() {
                let agent = ureq::Agent::new();
                let url = Url::parse(&login.url).unwrap();
                match minicaldav::get_calendars(agent.clone(), &login.login, &password, &url) {
                    Ok(cals) => {
                        debug!("Got calendars: {:?}", cals);
                        let mut synced_calendars = HashMap::new();
                        for cal in cals {
                            let calendar_url = cal.url().as_str().to_string();
                            let name = cal.name().clone();
                            let mut events = HashMap::new();
                            match minicaldav::get_events(
                                agent.clone(),
                                &login.login,
                                &password,
                                &cal,
                            ) {
                                Ok(evs) => {
                                    for ev in evs {
                                        events.insert(
                                            ev.url().as_str().to_string(),
                                            Event::from((calendar_url.clone(), ev)),
                                        );
                                    }
                                }
                                Err(e) => error!("{:?}", e),
                            };
                            debug!(
                                "Synced {} events for calendar {}.",
                                events.len(),
                                cal.name()
                            );
                            synced_calendars.insert(
                                calendar_url.clone(),
                                Calendar {
                                    id: calendar_url,
                                    name,
                                    events,
                                    is_remote: true,
                                    ..Default::default()
                                },
                            );
                            sender
                                .send(Sync::Progress)
                                .expect("Faild to send sync update");
                        }

                        Self::merge(&synced_calendars, &mut merged_calendars);

                        sender
                            .send(Sync::Success(merged_calendars))
                            .expect("Faild to send sync update");
                    }
                    Err(e) => {
                        error!("{:?}", e)
                    }
                }
            } else {
                sender
                    .send(Sync::Failure(gettext("No CalDAV login found!")))
                    .expect("Faild to send sync update");
                error!("Login not found!")
            }
        });
    }

    fn remove_event(event: &Event) {
        if let Some((login, password)) = Self::get_login() {
            let agent = ureq::Agent::new();
            if let Err(e) = minicaldav::remove_event(
                agent,
                &login.login,
                &password,
                minicaldav::Event::from(event),
            ) {
                error!("CalDAV Error: {:?}", e);
            }
        }
    }

    fn save_event(event: Event) -> Option<Event> {
        if let Some((login, password)) = Self::get_login() {
            let agent = ureq::Agent::new();
            if let Err(e) = minicaldav::save_event(
                agent,
                &login.login,
                &password,
                minicaldav::Event::from(&event),
            ) {
                error!("CalDAV Error: {:?}", e);
            }

            Some(event)
        } else {
            None
        }
    }
}

impl CaldavAdapter {
    fn merge(
        synced_calendars: &HashMap<String, Calendar>,
        merged_calendars: &mut HashMap<String, Calendar>,
    ) {
        for (synced_calendar_id, synced_calendar) in synced_calendars {
            merged_calendars.insert(synced_calendar_id.clone(), synced_calendar.clone());
        }
    }
}

// fn merge_calendar(sync_calendar: &Calendar, calendar: &mut Calendar) {
//     calendar.name = sync_calendar.name.clone();

//     for (sync_ev_url, sync_ev) in sync_calendar.events.iter() {
//         let mut event_merged_locally = false;
//         let mut remove_events = Vec::new();
//         for (event_url, event) in calendar.events.iter_mut() {
//             let mut event_exists_on_server = false;
//             if sync_ev_url == event_url {
//                 merge_event(sync_ev, event);
//                 event_merged_locally = true;
//                 event_exists_on_server = true;
//             }
//             if !event_exists_on_server {
//                 if SyncStatus::NotSynced == event.sync_status {
//                     // TODO: CaldavAdapter::save_event(event.clone()).unwrap();
//                 } else {
//                     remove_events.push(event_url.clone());
//                 }
//             }
//         }
//         for remove_event_id in remove_events {
//             calendar.events.remove(&remove_event_id);
//         }
//         if !event_merged_locally {
//             calendar.events.insert(sync_ev_url.clone(), sync_ev.clone());
//         }
//     }
// }

// fn merge_event(sync_event: &Event, event: &mut Event) {
//     match &event.sync_status {
//         SyncStatus::Synced(_) => {
//             set_event(event, sync_event);
//         }
//         SyncStatus::LocallyModified(local_version) => {
//             if let SyncStatus::Synced(remote_version) = &sync_event.sync_status {
//                 println!(
//                     "Comparing etags: {} ~ {} = {:?}",
//                     local_version,
//                     remote_version,
//                     remote_version.cmp(local_version)
//                 );
//                 if remote_version.cmp(local_version) == Ordering::Greater {
//                     // TODO: check if this is working
//                     set_event(event, sync_event);
//                 } else {
//                     // TODO: CaldavAdapter::save_event(event.clone());
//                 }
//             }
//         }
//         SyncStatus::LocallyDeleted(_) => {
//             // TODO: CaldavAdapter::remove_event(event);
//         }
//         SyncStatus::NotSynced => {
//             unreachable!("non-synced event should never be merged with a synced event.")
//         }
//     }
// }

// fn set_event(event: &mut Event, sync_event: &Event) {
//     event.name = sync_event.name.clone();
//     event.location = sync_event.location.clone();
//     event.full_day = sync_event.full_day;
//     event.start = sync_event.start.clone();
//     event.end = sync_event.end.clone();
//     event.repeat = sync_event.repeat.clone();
//     event.notes = sync_event.notes.clone();
//     event.sync_status = sync_event.sync_status.clone();
// }

impl From<&Event> for minicaldav::Event {
    fn from(event: &Event) -> Self {
        let etag = match &event.sync_status {
            SyncStatus::Synced(etag) => Some(etag),
            SyncStatus::LocallyModified(etag) => Some(etag),
            SyncStatus::LocallyDeleted(etag) => Some(etag),
            SyncStatus::NotSynced => None,
        };

        let repeat: Option<Vec<&(String, String)>> = if event.repeat.is_some() {
            let freq = event
                .repeat()
                .get(&RRULE_FIELD_FREQ.to_string())
                .cloned()
                .unwrap()
                .clone();
            if freq == RRULE_VALUE_WEEKLY {
                Some(
                    event
                        .repeat
                        .as_ref()
                        .unwrap()
                        .into_iter()
                        .filter(|(k, v)| {
                            k == RRULE_FIELD_BYDAY && !v.is_empty()
                                || k == RRULE_FIELD_FREQ
                                || k == RRULE_FIELD_COUNT
                                || k == RRULE_FIELD_UNTIL
                                || k == RRULE_FIELD_INTERVAL
                        })
                        .collect(),
                )
            } else {
                Some(
                    event
                        .repeat
                        .as_ref()
                        .unwrap()
                        .into_iter()
                        .filter(|(k, _)| {
                            k == RRULE_FIELD_FREQ
                                || k == RRULE_FIELD_COUNT
                                || k == RRULE_FIELD_UNTIL
                                || k == RRULE_FIELD_INTERVAL
                        })
                        .collect(),
                )
            }
        } else {
            None
        };

        let rrule: Option<String> = repeat.map(|r| {
            r.into_iter()
                .map(|(k, v)| format!("{}={}", k, v))
                .collect::<Vec<String>>()
                .join(";")
        });

        let mut date_attributes = vec![];
        let start;
        let end;
        if event.full_day {
            date_attributes.push(("VALUE", "DATE"));
            start = event.start.format(DatetimeFormat::ISO8601DATE);
            end = event.end.format(DatetimeFormat::ISO8601DATE);
        } else {
            start = event.start.format(DatetimeFormat::ISO8601Z);
            end = event.end.format(DatetimeFormat::ISO8601Z);
        }
        let tz = KarlenderTimezone::local();
        let builder = minicaldav::Event::builder(Url::parse(&event.item_url).unwrap())
            .etag(etag.cloned())
            .uid(event.id.clone())
            .summary(event.name.clone())
            .location(event.location.clone())
            .description(event.notes.clone())
            .start(start, date_attributes.clone())
            .end(end, date_attributes)
            .rrule(rrule)
            .timestamp(tz.to_utc(&tz.now()).format(DatetimeFormat::ISO8601Z));
        let event = builder.build();
        debug!("Created ICAL Event: {:#?}", event);
        event
    }
}

impl From<(String, minicaldav::Event)> for Event {
    fn from((calendar_id, event): (String, minicaldav::Event)) -> Self {
        let mut tz = None;
        let start = event.property("DTSTART").unwrap();
        if let Some(tz_name) = start.attribute("TZID") {
            let tz_name = tz_name.replace('-', "/");
            tz = KarlenderTimezone::from_str(&tz_name).ok();
        }
        let end = event.property("DTEND").unwrap();
        if tz.is_none() {
            if let Some(tz_name) = end.attribute("TZID") {
                let tz_name = tz_name.replace('-', "/");
                tz = KarlenderTimezone::from_str(&tz_name).ok();
            }
        }

        let (start, full_day) =
            KarlenderDateTime::from_iso8601(start.value(), tz.as_ref()).unwrap();
        let (end, _) = KarlenderDateTime::from_iso8601(end.value(), tz.as_ref()).unwrap();

        let name = event
            .property("SUMMARY")
            .map(|p| p.value().clone())
            .unwrap_or_else(|| "Unnamed".to_string());

        if name == "Test" || name == "Test2" {
            debug!("Parse ICAL Event: {:#?}", event);
        }

        let start = if let Some(tz) = &tz {
            tz.to_utc(&start)
        } else {
            start
        };

        let end = if let Some(tz) = &tz {
            tz.to_utc(&end)
        } else {
            end
        };

        Self {
            id: event.url().as_str().to_string(),
            calendar_url: calendar_id,
            item_url: event.url().as_str().to_string(),
            name,
            location: event.get("LOCATION").cloned(),
            start,
            end,
            full_day,
            notes: event.property("DESCRIPTION").map(|p| p.value().clone()),
            repeat: event.property("RRULE").map(|p| p.value().clone()).map(|d| {
                d.split(';')
                    .filter_map(|kv| kv.split_once('='))
                    .map(|(k, v)| (k.to_string(), v.to_string()))
                    .collect()
            }),
            sync_status: SyncStatus::Synced(event.etag().unwrap().clone()),
        }
    }
}
