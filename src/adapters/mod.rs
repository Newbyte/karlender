pub mod caldav;

pub trait Adapter {
    fn init();
}

pub use caldav::*;
