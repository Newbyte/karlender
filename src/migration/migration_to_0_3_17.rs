use semver::VersionReq;
use serde_json::Value;

const VERSION_REQ: &str = "<0.3.17";

pub fn migrate(json: &mut Value) {
    let version = super::get_version(&json);
    let version_check = VersionReq::parse(VERSION_REQ).unwrap();
    debug!(
        "Check migration for version {} with {}",
        version,
        &VERSION_REQ[1..]
    );
    if version_check.matches(&version) {
        let new_version = VERSION_REQ[1..].into();
        debug!("Migrate from \"{}\" to {}", version, new_version);
        json["version"] = new_version;
        let calendars = json["calendar_slice"]["calendars"].as_object_mut().unwrap();
        for (_cid, calendar) in calendars {
            calendar["is_remote"] = true.into();
        }
    }
}
