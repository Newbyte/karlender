use crate::adapters::{Adapter, CaldavAdapter, CaldavAdapterApi};
use crate::domain::*;
use crate::store::{
    gstore::{self, dispatch},
    store, State,
};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::sync::Mutex;

#[derive(Default, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct CalendarsSlice {
    pub calendars: HashMap<CalendarId, Calendar>,
    pub last_sync: Option<KarlenderDateTime>,
}

impl std::fmt::Debug for CalendarsSlice {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_list().entries(self.calendars.values()).finish()
    }
}

pub static SYNCED_CALENDARS: gstore::once_cell::sync::OnceCell<
    Mutex<Option<HashMap<String, Calendar>>>,
> = gstore::once_cell::sync::OnceCell::new();

pub fn reduce(action: &gstore::Action, state: &mut State) {
    match action.name() {
        crate::store::_ADD_CALENDAR => {
            let new_calendar = Calendar::default();
            state
                .calendar_slice
                .calendars
                .insert(new_calendar.id.clone(), new_calendar);
        }
        crate::store::_REMOVE_CALENDAR => {
            let calendar_id: String = action.arg().unwrap();
            state.calendar_slice.calendars.remove(&calendar_id);
        }
        crate::store::UPDATE_CALENDAR => {
            let (cal_id, props): (String, HashMap<String, glib::Variant>) = action.arg().unwrap();

            let enabled = props
                .get(CALENDAR_PROP_ENABLED)
                .and_then(|v| v.get::<bool>());
            let default = props
                .get(CALENDAR_PROP_DEFAULT)
                .and_then(|v| v.get::<bool>());
            let color = props
                .get(CALENDAR_PROP_COLOR)
                .and_then(|v| v.get::<String>());
            let name = props
                .get(CALENDAR_PROP_NAME)
                .and_then(|v| v.get::<String>());

            if let Some(default) = default {
                if default {
                    for c in &mut state.calendar_slice.calendars.values_mut() {
                        c.default = false;
                    }
                }
            }
            if let Some(name) = name {
                let cal = state.calendar_slice.calendars.get_mut(&cal_id).unwrap();
                if !cal.is_remote {
                    cal.name = name;
                }
            }
            if let Some(cal) = state.calendar_slice.calendars.get_mut(&cal_id) {
                if let Some(default) = default {
                    cal.default = default;
                }
                if let Some(enabled) = enabled {
                    cal.enabled = enabled;
                }
                if let Some(color) = color {
                    cal.color = KarlenderColor::from(color);
                }
            }
        }
        crate::store::_SYNC_CALENDARS_DONE => {
            let error: Option<String> = action.arg().unwrap();
            if let Some(message) = error {
                state.ui.in_app_notification =
                    Some(format!("Could not sync calendars: {}", message));
                return;
            }
            let synced_calendars = SYNCED_CALENDARS.get();
            if let Some(synced_calendars) = synced_calendars {
                let mut synced_calendars = synced_calendars.lock().unwrap();
                state.calendar_slice.calendars = synced_calendars.take().unwrap();
                state.calendar_slice.last_sync = Some(KarlenderTimezone::local().now());
            } else {
                state.ui.in_app_notification = Some("Could not sync calendars".into());
            }
        }
        // crate::store::SAVE_EVENT => {
        // let editor = &mut state.editor;
        // let calendar_slice = &mut state.calendar_slice;

        // if let Some(_event) = editor.event.take() {
        // calendar_slice
        //     .calendars
        //     .get_mut(&event.calendar_url)
        //     .unwrap()
        //     .events
        //     .insert(event.id.clone(), event);
        // }
        // }
        crate::store::REMOVE_EVENT => {
            let calendars_slice = &mut state.calendar_slice;
            let (calendar_id, event_id): (String, String) = action.arg().unwrap();

            if let Some(calendar) = calendars_slice.calendars.get_mut(&calendar_id) {
                calendar.events.remove(&event_id);
            }
        }
        _ => {}
    }
}

#[derive(Default)]
pub struct CalDAVSyncMiddleware;
impl CalDAVSyncMiddleware {
    pub(crate) fn new() -> Box<Self> {
        Box::new(CalDAVSyncMiddleware::default())
    }
}

impl gstore::Middleware<crate::store::State> for CalDAVSyncMiddleware {
    fn pre_reduce(&self, action: &gstore::Action, state: &crate::store::State) {
        match action.name() {
            crate::store::SYNC => {
                info!("[calendars middleware] SYNC");
                let (sender, receiver) = glib::MainContext::channel::<crate::adapters::caldav::Sync>(
                    glib::PRIORITY_DEFAULT,
                );

                receiver.attach(None, |sync| {
                    match sync {
                        crate::adapters::caldav::Sync::Started => {
                            // nothing to do
                        }
                        crate::adapters::caldav::Sync::Progress => {
                            // nothing to do
                        }
                        crate::adapters::caldav::Sync::Success(calendars) => {
                            let c = SYNCED_CALENDARS.get_or_init(Mutex::default);
                            let mut c = c.lock().unwrap();
                            *c = Some(calendars);
                            drop(c);
                            dispatch!(crate::store::_SYNC_CALENDARS_DONE, None::<String>);
                            return glib::Continue(false);
                        }
                        crate::adapters::caldav::Sync::Failure(message) => {
                            dispatch!(crate::store::_SYNC_CALENDARS_DONE, Some(message));
                            return glib::Continue(false);
                        }
                    }
                    glib::Continue(true)
                });
                CaldavAdapter::sync(sender, &state.calendar_slice.calendars);
            }
            crate::store::REMOVE_EVENT => {
                let calendars_slice = &state.calendar_slice;
                let (calendar_id, event_id): (String, String) = action.arg().unwrap();

                if let Some(calendar) = calendars_slice.calendars.get(&calendar_id) {
                    if !calendar.is_remote {
                        return;
                    }
                    if let Some(event) = calendar.events.get(&event_id) {
                        CaldavAdapter::remove_event(event);
                    }
                }
            }
            _ => {}
        }
    }

    fn post_reduce(&self, action: &gstore::Action, state: &crate::store::State) {
        match action.name() {
            crate::store::gstore::INIT => {
                CaldavAdapter::init();
                if let Some((login, password)) = CaldavAdapter::get_login() {
                    // Do not send the password around. It must not be logged.
                    let mutex = LOGIN_PASSWORD.get_or_init(Default::default);
                    let mut lock = mutex.lock().unwrap();
                    *lock = Some(password);
                    drop(lock);

                    dispatch!(crate::store::LOGIN, (login.url, login.login));
                }
            }

            crate::store::LOGIN => {
                let (url, login): (String, String) = action.arg().unwrap();
                if let Some(mutex) = LOGIN_PASSWORD.get() {
                    if let Ok(mut lock) = mutex.lock() {
                        if let Some(password) = lock.take() {
                            CaldavAdapter::login(url, login, password);
                        }
                    }
                }
            }

            crate::store::REMOVE_ACCOUNT => {
                let (_url, _login): (String, String) = action.arg().unwrap();
                CaldavAdapter::remove_login();
            }

            crate::store::_INTERNAL_SAVE_EVENT => {
                let event: Event = action.arg().unwrap();
                if let Some(calendar) = state.calendar_slice.calendars.get(&event.calendar_url) {
                    if calendar.is_remote {
                        if let Some(created_event) = CaldavAdapter::save_event(event) {
                            debug!("Updated event in remote: {:?}", created_event);
                            dispatch!(crate::store::SYNC);
                        }
                    }
                } else {
                    error!("Could not find calendar for event {:?}", event);
                }
            }

            crate::store::REMOVE_EVENT => {
                let (cal_id, _event_id): (String, String) = action.arg().unwrap();
                if let Some(calendar) = state.calendar_slice.calendars.get(&cal_id) {
                    if calendar.is_remote {
                        dispatch!(crate::store::SYNC);
                    }
                }
            }
            _ => {}
        }
    }
}

pub static LOGIN_PASSWORD: gstore::once_cell::sync::OnceCell<Mutex<Option<String>>> =
    gstore::once_cell::sync::OnceCell::new();
