use crate::domain::*;
use crate::store::{
    gstore::{self, dispatch},
    store, State,
};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct Selection {
    pub start: KarlenderDateTime,
    pub end: KarlenderDateTime,
    pub full_day: bool,
    pub selected: bool,
    pub num_clicks: i32,
}

impl Selection {
    pub fn new(
        start: KarlenderDateTime,
        end: KarlenderDateTime,
        full_day: bool,
        selected: bool,
        num_clicks: i32,
    ) -> Self {
        Self {
            start,
            end,
            full_day,
            selected,
            num_clicks,
        }
    }

    pub fn to_variant(&self) -> (String, String, bool, bool, i32) {
        (
            self.start.to_string(),
            self.end.to_string(),
            self.full_day,
            self.selected,
            self.num_clicks,
        )
    }

    pub fn from_variant(
        (start, end, full_day, selected, num_clicks): (String, String, bool, bool, i32),
    ) -> Self {
        Self {
            start: KarlenderDateTime::from_str(&start).unwrap(),
            end: KarlenderDateTime::from_str(&end).unwrap(),
            full_day,
            selected,
            num_clicks,
        }
    }
}

#[derive(Debug, Default, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct SelectionSlice {
    pub former_selection: Option<Selection>,
    pub selection: Option<Selection>,
    pub view_events: Vec<(CalendarId, EventId)>,
}

#[derive(Debug, Default)]
pub(crate) struct SelectionMiddleware;
impl SelectionMiddleware {
    pub(crate) fn new() -> Box<Self> {
        Box::new(SelectionMiddleware::default())
    }
}

impl gstore::Middleware<crate::store::State> for SelectionMiddleware {
    fn post_reduce(&self, action: &gstore::Action, state: &crate::store::State) {
        if action.name() == crate::store::SELECT {
            let new_selection = Selection::from_variant(action.arg().unwrap());
            let view_events = !state.selection_slice.view_events.is_empty();
            let mobile = state.ui.mobile;
            let selection = &state.selection_slice.selection;
            if view_events && (mobile || new_selection.num_clicks == 2) {
                dispatch!(crate::store::NAVIGATE, crate::pages::DAY_PAGE_NAME);
            } else if selection.is_none() && (mobile || new_selection.num_clicks == 2) {
                dispatch!(crate::store::NEW_EVENT);
            }
        } else if action.name() == crate::store::NEW_EVENT && state.ui.in_app_notification.is_none()
        {
            dispatch!(crate::store::NAVIGATE, crate::pages::EDITOR_PAGE_NAME);
        }
    }
}

pub(crate) fn reduce(action: &gstore::Action, state: &mut State) {
    match action.name() {
        crate::store::_GO_PREV => {
            debug!("pre: {:?}", state.selection_slice);
            if state.selection_slice.selection.is_none() {
                state.selection_slice.selection = state.selection_slice.former_selection.take();
            }
            if let Some(selection) = &mut state.selection_slice.selection {
                selection.start = selection.start.clone() - KarlenderDateTimeDuration::days(1);
                selection.end = selection.end.clone() - KarlenderDateTimeDuration::days(1);
                if state.ui.mobile {
                    state.ui.title = selection.start.date().format(DatetimeFormat::DateBottomUp)
                }
            }
            debug!("post: {:?}", state.selection_slice);
        }
        crate::store::_GO_NEXT => {
            debug!("pre: {:?}", state.selection_slice);
            if state.selection_slice.selection.is_none() {
                state.selection_slice.selection = state.selection_slice.former_selection.take();
            }
            if let Some(selection) = &mut state.selection_slice.selection {
                selection.start = selection.start.clone() + KarlenderDateTimeDuration::days(1);
                selection.end = selection.end.clone() + KarlenderDateTimeDuration::days(1);
                if state.ui.mobile {
                    state.ui.title = selection.start.date().format(DatetimeFormat::DateBottomUp)
                }
            }
            debug!("post: {:?}", state.selection_slice);
        }

        crate::store::SELECT => {
            debug!("pre: {:?}", state.selection_slice);
            let new_selection = Selection::from_variant(action.arg().unwrap());
            state.selection_slice.former_selection = state.selection_slice.selection.take();

            if state.ui.mobile {
                if !new_selection.selected {
                    state.selection_slice.selection = None;
                    let mut view_events = Vec::new();
                    for calendar in state.calendar_slice.calendars.values() {
                        for event in calendar.events.values() {
                            if event
                                .is_between(new_selection.start.clone(), new_selection.end.clone())
                                || (new_selection.full_day
                                    && event.contains_date(&new_selection.start.date()))
                            {
                                view_events.push((calendar.id.clone(), event.id.clone()));
                            }
                        }
                    }
                    state.selection_slice.view_events = view_events;
                } else {
                    state.selection_slice.selection = Some(new_selection);
                    state.selection_slice.view_events.clear();
                }
            } else if !new_selection.selected {
                state.selection_slice.selection = None;
            } else {
                let mut view_events = Vec::new();
                for calendar in state.calendar_slice.calendars.values() {
                    for event in calendar.events.values() {
                        if event.is_between(new_selection.start.clone(), new_selection.end.clone())
                            || (new_selection.full_day
                                && event.contains_date(&new_selection.start.date()))
                        {
                            view_events.push((calendar.id.clone(), event.id.clone()));
                        }
                    }
                }
                state.selection_slice.view_events = view_events;
                state.selection_slice.selection = Some(new_selection);
            }
            debug!("post: {:?}", state.selection_slice);
        }

        _ => {}
    }
}
