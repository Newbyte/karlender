# Roadmap

## Version 1.0.0

### Functional

- [x] View events from CalDAV calendars
- [x] Edit events from CalDAV calendars
  - [x] Support name, location and notes
  - [x] Support start and end date
  - [x] Support full-day events and timeslot events
  - [ ] Support repeated events
  - [ ] Support reminder
- [x] Delete events from CalDAV calendars
- [x] Add events to CalDAV calendars
- [x] Monthly view for calendars

### Non-Functional

- Karlender is as easy to use via touch as it is via mouse and keyboard
- Karlender has a small footprint that makes it usable on the pinephone
- Dark and Light themes work as well as high contrast mode

## Later

- [ ] Weekly view
- [ ] Yearly view
